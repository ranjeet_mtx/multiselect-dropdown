# DESCRIPTION #
- Util component for multiselect-picklist.
- Allow to "Search" in Picklist Values.

# HOW TO ENABLE SEARCH #
- To enable SEARCH Feature, pass "allow-search=true" while calling component.

# REQUIRED VALIDTION #
- Use "validateValue()" api function to check for validity.
@Note - Validation function will be called on each selection and de-selction.

# HOW TO USE #
<c-multiselect_picklist name="multiselect_picklist" 
    all-options={allOptions} 
    selected-options={selectedOtions} 
    allow-search=true
    is-required=true
    onselected={handleOptionSelection}>
</c-multiselect_picklist>
